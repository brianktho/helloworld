#Production environment
FROM node:6.7
ENV NODE_ENV production
ENV HOME=/root
WORKDIR $HOME/app

COPY app.js $HOME/app/
COPY package.json $HOME/app/
COPY yarn.lock $HOME/app/
RUN ["npm", "install"]

CMD ["npm", "start"]

EXPOSE 3000
